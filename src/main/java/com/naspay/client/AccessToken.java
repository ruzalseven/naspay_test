package com.naspay.client;

import com.naspay.client.excp.NaspayAccessTokenExpiredException;
import com.naspay.client.excp.NaspayException;

public class AccessToken {
    private final long expiresOn;
    private final long maxUnuseMills;
    private final String value;
    private long lastUsage;

    public AccessToken(String value, long liveMills, long maxUnuseMills) {
        this.expiresOn = System.currentTimeMillis() + liveMills;
        this.lastUsage = System.currentTimeMillis();
        this.maxUnuseMills = maxUnuseMills;
        if (value == null) {
            throw new NaspayException("Access token is null!");
        }
        this.value = value;

    }

    public String getValue() {
        long currentTime = System.currentTimeMillis();
        if (expiresOn <= currentTime || lastUsage + maxUnuseMills <= currentTime) {
            throw new NaspayAccessTokenExpiredException("Token is expired");
        }
        lastUsage = System.currentTimeMillis();
        return value;
    }
}
