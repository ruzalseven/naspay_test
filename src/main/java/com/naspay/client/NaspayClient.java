package com.naspay.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.naspay.client.excp.NaspayAccessTokenExpiredException;
import com.naspay.client.excp.NaspayAuthException;
import com.naspay.client.excp.NaspayException;
import com.naspay.client.excp.NaspayUnauthorizedException;
import com.naspay.client.response.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Base64;
import java.util.function.Supplier;

public class NaspayClient {
    private final Object mutex = new Object();
    private final String terminalKey;
    private final String terminalSecret;
    private final long tokenLiveMills = 8 * 60 * 60 * 1000; //8 hours
    private final long tokenMaxUnuseMills = 15 * 60 * 1000; //15 minutes
    private final Retry retry = Retry.of(3);
    private volatile AccessToken accessToken;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final String API_DOMAIN = "https://demo.naspay.net";
    private static final String API_URL = API_DOMAIN + "/api/v1";
    private static final String API_TRANSACTIONS_ENDPOINT = API_URL + "/transactions/";
    private static final String API_AUTH_URL = API_DOMAIN + "/auth/token";
    private static final String AUTH_HEADER = "Authorization";

    private final Logger log = LoggerFactory.getLogger(NaspayClient.class);

    public NaspayClient(String terminalKey, String terminalSecret) {
        this.terminalKey = terminalKey;
        this.terminalSecret = terminalSecret;

    }

    public String getTransactionStatus(String transactionId) {
        return retry.exec(() -> {
            try (CloseableHttpClient client = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(API_TRANSACTIONS_ENDPOINT + transactionId);
                httpGet.addHeader("accept", "application/json");
                httpGet.addHeader(AUTH_HEADER, "Bearer " + getAccessToken());

                CloseableHttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                switch (statusCode) {
                    case 200:
                        TransactionDetailsResponse result = mapPayload(response, TransactionDetailsResponse.class);
                        return result.getState();
                    case 401:
                        UnauthorizedErrorResponse unauthorizedError = mapPayload(response, UnauthorizedErrorResponse.class);
                        throw new NaspayUnauthorizedException("Unauthorized error: " + unauthorizedError.getError() + " message: " + unauthorizedError.getMessage());
                    case 400:
                        ErrorResponse error = mapPayload(response, ErrorResponse.class);
                        throw new NaspayException(error.getError().getDescription(), error.getError().getCode());

                    case 404:
                        throw new NaspayException("Transaction is not found");
                }
            } catch (IOException e) {
                throw new NaspayException("Request failed", e);
            }
            throw new NaspayException("Fail on get transaction");
        });
    }

    private String getAccessToken() throws NaspayException {
        if (accessToken == null) {
            synchronized (mutex) {
                if (accessToken == null) {
                    this.accessToken = auth();
                }
            }
        }
        try {
            return accessToken.getValue();
        } catch (NaspayAccessTokenExpiredException e) {
            log.debug("Access token is expired, try to refresh");
            synchronized (mutex) {
                this.accessToken = auth();
            }
            return accessToken.getValue();
        }
    }

    private AccessToken auth() throws NaspayException {
        return retry.exec(() -> {
            try (CloseableHttpClient client = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(API_AUTH_URL + "?grant_type=transactions");
                httpGet.addHeader("accept", "application/json");
                httpGet.addHeader(AUTH_HEADER, "Basic " + getEncodedCreds());

                CloseableHttpResponse response = client.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    AccessTokenResponse result = mapPayload(response, AccessTokenResponse.class);
                    return new AccessToken(result.getAccessToken(), tokenLiveMills, tokenMaxUnuseMills);
                } else {
                    AuthErrorResponse error = mapPayload(response, AuthErrorResponse.class);
                    if (error != null) {
                        throw new NaspayAuthException("Auth is failed: " + error.getMessage());
                    }
                    throw new NaspayAuthException("Auth is failed, with status code : " + statusCode);
                }
            } catch (IOException e) {
                throw new NaspayAuthException("Auth is failed", e);
            }
        });
    }

    private String getEncodedCreds() {
        return new String(Base64.getEncoder().encode((terminalKey + ":" + terminalSecret).getBytes()));
    }

    private <T> T mapPayload(HttpResponse response, Class<T> type) throws IOException {
        return objectMapper.readValue(response.getEntity().getContent(), type);
    }

    private static class Retry {
        private int retries;

        private Retry() {
        }

        private static Retry of(int retries) {
            Retry retry = new Retry();
            retry.retries = retries;
            return retry;
        }

        public <T> T exec(Supplier<T> supplier) {
            NaspayException lastException = new NaspayException("Retry failed");
            int retryLocal = retries;
            while (retryLocal > 0) {
                try {
                    retryLocal--;
                    return supplier.get();
                } catch (NaspayUnauthorizedException e) {//TODO add another cases to retry
                    lastException = e;
                }
            }
            throw lastException;
        }
    }
}
