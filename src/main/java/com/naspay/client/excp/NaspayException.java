package com.naspay.client.excp;

public class NaspayException extends RuntimeException {
    private String errorCode;

    public NaspayException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public NaspayException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public NaspayException(String message) {
        super(message);
    }

    public NaspayException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
