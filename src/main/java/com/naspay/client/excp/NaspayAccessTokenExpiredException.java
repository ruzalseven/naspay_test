package com.naspay.client.excp;

import com.naspay.client.response.ErrorResponse;

public class NaspayAccessTokenExpiredException extends NaspayException {
    public NaspayAccessTokenExpiredException(ErrorResponse errorResponse, String message, String errorCode) {
        super(message, errorCode);
    }

    public NaspayAccessTokenExpiredException(ErrorResponse errorResponse, String message, Throwable cause, String errorCode) {
        super(message, cause, errorCode);
    }

    public NaspayAccessTokenExpiredException(String message) {
        super(message);
    }

    public NaspayAccessTokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
