package com.naspay.client.excp;

import com.naspay.client.response.ErrorResponse;

public class NaspayAuthException extends NaspayException {
    public NaspayAuthException(ErrorResponse errorResponse, String message, String errorCode) {
        super(message, errorCode);
    }

    public NaspayAuthException(ErrorResponse errorResponse, String message, Throwable cause, String errorCode) {
        super(message, cause, errorCode);
    }

    public NaspayAuthException(String message) {
        super(message);
    }

    public NaspayAuthException(String message, Throwable cause) {
        super(message, cause);
    }
}
