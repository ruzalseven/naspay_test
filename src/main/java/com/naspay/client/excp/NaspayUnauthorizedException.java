package com.naspay.client.excp;

import com.naspay.client.response.ErrorResponse;

public class NaspayUnauthorizedException extends NaspayException {
    public NaspayUnauthorizedException(ErrorResponse errorResponse, String message, String errorCode) {
        super(message, errorCode);
    }

    public NaspayUnauthorizedException(ErrorResponse errorResponse, String message, Throwable cause, String errorCode) {
        super(message, cause, errorCode);
    }

    public NaspayUnauthorizedException(String message) {
        super(message);
    }

    public NaspayUnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
