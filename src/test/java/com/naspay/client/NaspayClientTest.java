package com.naspay.client;

import com.naspay.client.excp.NaspayAuthException;
import com.naspay.client.excp.NaspayException;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NaspayClientTest {
    private static final String TERMINAL_KEY = "api-demo";
    private static final String TERMINAL_SECRET = "test123";

    @Test
    public void getTransactionStatus_AUTHORIZED() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("AUTHORIZED", client.getTransactionStatus("dbb46d0c4cfc4cdd9f923aa229106287"));
    }

    @Test
    public void getTransactionStatus_DECLINED() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("DECLINED", client.getTransactionStatus("5ce5fbd950294c4cbf3f3276e645668a"));
    }

    @Test
    public void getTransactionStatus_TIMEOUT() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("TIMEOUT", client.getTransactionStatus("8a0885e6526c4257ba213292a0d956b0"));
    }

    @Test
    public void getTransactionStatus_COMPLETED() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("COMPLETED", client.getTransactionStatus("1261481f9625430887e155817e45ec33"));
    }

    @Ignore //todo this transaction is not found in the api
    @Test
    public void getTransactionStatus_Handle() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("Handle", client.getTransactionStatus("f33ae3006b074e649c1095ff8fb35e93"));
    }

    @Test(expected = NaspayException.class)
    public void getTransactionStatus_NotFound() {
        NaspayClient client = new NaspayClient(TERMINAL_KEY, TERMINAL_SECRET);
        assertEquals("ANY", client.getTransactionStatus("NOT_EXITS_ID"));
    }

    @Test(expected = NaspayAuthException.class)
    public void getTransactionStatus_BadCreds() {
        NaspayClient client = new NaspayClient("blabla", "blabla");
        assertEquals("COMPLETED", client.getTransactionStatus("1261481f9625430887e155817e45ec33"));
    }
}
